package com.aihsoftware.tfl.view

import com.aihsoftware.tfl.ApiService
import com.aihsoftware.tfl.presenter.MVPContract
import com.aihsoftware.tfl.presenter.TflPresenter
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test


class MainActivityUnitTests {

    private lateinit var presenter: TflPresenter
    private lateinit var view: MVPContract.ViewContract
    private lateinit var apiService: ApiService

    @Before
    fun setup() {


        apiService = mock()
        presenter = TflPresenter(apiService)
        view = mock()
        presenter.attachView(view)

    }

    @Test
    fun search_WithValidRoadName_CheckCallsGetRoad_CheckCallsUpdateDetailsView() {

        CoroutineScope(Dispatchers.IO) .launch{

            presenter.submitSearch("A2")
            verify(apiService).getRoad("")
            verify(view).updateDetailsView(null, null)

        }

    }
    @Test
    fun search_WithInvalidRoadName_CheckCallsGetRoad_CheckCallsUpdateDetailsView() {

        CoroutineScope(Dispatchers.IO) .launch{

            presenter.submitSearch("A2")
            verify(apiService).getRoad("")
            verify(view).updateDetailsView(null, null)

        }

    }

}