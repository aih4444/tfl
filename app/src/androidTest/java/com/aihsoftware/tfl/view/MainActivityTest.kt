package com.aihsoftware.tfl.view

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.aihsoftware.tfl.R

import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Test
    fun appLaunchesSuccessfully() {
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun checkSearchInputIsDisplayedAndContainsHint() {
        appLaunchesSuccessfully()
        onView(withId(R.id.input_search)).check(matches(isDisplayed()))
        onView(withId(R.id.input_search)).check(matches(withHint(R.string.enter_road_name_hint)))
    }

    @Test
    fun insertValidRoadNameAndClickSearchButtonAndCheckResults() {
        appLaunchesSuccessfully()

        onView(withId(R.id.input_search)).perform(click()).perform(
            typeTextIntoFocusedView(
                getStringResource(
                    R.string.test_value_input_search_road_name_valid
                )
            ),ViewActions.closeSoftKeyboard()

        )
        performClickandCheckValues("A2","Good","No Exceptional Delays")
    }

    @Test
    fun insertInvalidRoadNameAndClickSearchButtonAndCheckResults() {
        appLaunchesSuccessfully()

        onView(withId(R.id.input_search)).perform(click()).perform(
            typeTextIntoFocusedView(
                getStringResource(
                    R.string.test_value_input_search_road_name_invalid
                )
            ),ViewActions.closeSoftKeyboard()

        )
        performClickandCheckValues("A233","NotFound","The following road id is not recognised: A233")
    }

    private fun performClickandCheckValues(displayName:String,statusSeverity:String,statusSeverityDescription:String) {
        onView(withId(R.id.search_button)).perform(click())
        onView(withId(R.id.output_display_name)).check(matches(withText(displayName)))
        onView(withId(R.id.output_status_severity)).check(matches(withText(statusSeverity)))
        onView(withId(R.id.output_status_severity_description)).check(matches(withText(statusSeverityDescription)))
    }

    fun getStringResource(id: Int): String {
        val targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        return targetContext.resources.getString(id)
    }
}