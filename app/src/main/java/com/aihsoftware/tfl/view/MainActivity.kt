package com.aihsoftware.tfl.view


import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.aihsoftware.tfl.databinding.ActivityMainBinding
import com.aihsoftware.tfl.model.TflRoadErrorResponse
import com.aihsoftware.tfl.model.TflRoadResponse
import com.aihsoftware.tfl.presenter.MVPContract
import com.aihsoftware.tfl.presenter.TflPresenter
import kotlinx.coroutines.runBlocking
import org.koin.android.ext.android.inject

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity(), MVPContract.ViewContract {

    val presenter: MVPContract.PresenterContract by inject()

    var _binding: ActivityMainBinding? = null
    val binding: ActivityMainBinding
        get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate: test")
        _binding = ActivityMainBinding.inflate(layoutInflater)
        val root = binding.root
        binding.lifecycleOwner = this
        setContentView(root)
        (presenter as TflPresenter).attachView(this)


        binding.searchButton.setOnClickListener {
            runBlocking {
                val inputText = binding.inputSearch.text.trim().toString()
                binding.inputSearch.text.clear()
                binding.inputSearch.hint = ""
                presenter.submitSearch(inputText)
            }
        }

    }

    override fun updateDetailsView(
        tflRoadResponse: TflRoadResponse?,
        tflRoadResponseError: TflRoadErrorResponse?
    ) {
        binding.outputDisplayName.text =
            tflRoadResponse?.displayName ?: tflRoadResponseError?.relativeUri?.replace("/Road/", "")
                ?.split('?')?.get(0)
        binding.outputStatusSeverity.text =
            tflRoadResponse?.statusSeverity ?: tflRoadResponseError?.httpStatus
        binding.outputStatusSeverityDescription.text =
            tflRoadResponse?.statusSeverityDescription ?: tflRoadResponseError?.message


    }


}