package com.aihsoftware.tfl.di

import com.aihsoftware.tfl.Constants
import com.aihsoftware.tfl.network.TflResponseInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitModule = module {
    fun provideModule(gson: Gson, okHttpClient: OkHttpClient) =
        Retrofit.Builder().baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory()).client(okHttpClient)
            .build()

    fun provideOKHttpClient() =
        OkHttpClient.Builder()/*.addInterceptor(HttpLoggingInterceptor().apply {level = HttpLoggingInterceptor.Level.BODY })*/
            .addInterceptor(TflResponseInterceptor()).build()

    fun provideGson(): Gson = GsonBuilder().create()

    single { provideGson() }
    single { provideOKHttpClient() }
    single { provideModule(get(), get()) }
}


