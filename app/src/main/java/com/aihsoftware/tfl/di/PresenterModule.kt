package com.aihsoftware.tfl.di

import com.aihsoftware.tfl.ApiService
import com.aihsoftware.tfl.presenter.MVPContract
import com.aihsoftware.tfl.presenter.TflPresenter
import org.koin.dsl.module

val presenterModule = module {
    fun provideModule(apiService: ApiService):MVPContract.PresenterContract = TflPresenter(apiService)

    single { provideModule(get()) }
}