package com.aihsoftware.tfl.di

import com.aihsoftware.tfl.ApiService
import org.koin.dsl.module
import retrofit2.Retrofit

val apiService = module {
    fun provideModule(retrofit: Retrofit): ApiService = retrofit.create(
        ApiService::class.java)

    single{provideModule(get())}
}