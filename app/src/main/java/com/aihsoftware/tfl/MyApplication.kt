package com.aihsoftware.tfl

import android.app.Application


import com.aihsoftware.tfl.di.apiService
import com.aihsoftware.tfl.di.presenterModule
import com.aihsoftware.tfl.di.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level



class MyApplication: Application() {


    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@MyApplication)
            androidLogger(Level.DEBUG)
            modules(listOf(retrofitModule,
                apiService, presenterModule
            ))
        }

    }


}