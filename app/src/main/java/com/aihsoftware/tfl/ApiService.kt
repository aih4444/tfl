package com.aihsoftware.tfl


import com.google.gson.JsonElement
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("${Constants.ROAD}/{roadName}")
    suspend fun getRoad(@Path("roadName") roadName: String,@Query("API_KEY") apikey : String = BuildConfig.API_KEY): JsonElement//List<TflRoadResponse>

}