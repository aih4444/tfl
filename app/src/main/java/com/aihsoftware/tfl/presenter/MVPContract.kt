package com.aihsoftware.tfl.presenter

import com.aihsoftware.tfl.model.TflRoadErrorResponse
import com.aihsoftware.tfl.model.TflRoadResponse

interface MVPContract {
    interface PresenterContract {
        suspend fun submitSearch(searchRoad: String)
    }

    interface ViewContract {
        fun updateDetailsView(tflRoadResponse: TflRoadResponse?, tflRoadResponseError: TflRoadErrorResponse?)

    }
}