package com.aihsoftware.tfl.presenter


import com.aihsoftware.tfl.ApiService
import com.aihsoftware.tfl.model.TflRoadErrorResponse
import com.aihsoftware.tfl.model.TflRoadResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.HttpURLConnection

private const val TAG = "TflPresenter"

class TflPresenter(val apiService: ApiService) : MVPContract.PresenterContract {

    private var _view: MVPContract.ViewContract? = null
    val view: MVPContract.ViewContract
        get() = _view!!

    fun attachView(view: MVPContract.ViewContract) {
        _view = view
    }

    override suspend fun submitSearch(searchRoad: String) {

        var tflRoadResponse: TflRoadResponse? = null
        var tflRoadErrorResponse: TflRoadErrorResponse? = null

        val job = CoroutineScope(Dispatchers.IO).launch {

            var response: JsonElement

            response = apiService.getRoad(searchRoad)


            if (response.toString()
                    .contains(HttpURLConnection.HTTP_NOT_FOUND.toString()) || response.toString()
                    .contains("NotFound")
            ) {
                tflRoadErrorResponse = Gson().fromJson(response, TflRoadErrorResponse::class.java)
            } else {
                val token = object : TypeToken<List<TflRoadResponse>>() {}.type
                var something: List<TflRoadResponse> = Gson().fromJson(response, token)
                tflRoadResponse = something[0]
            }


        }
        job.join()
        view.updateDetailsView(tflRoadResponse, tflRoadErrorResponse)
    }
}