package com.aihsoftware.tfl.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import java.net.HttpURLConnection

private const val TAG = "TflResponseInterceptor"

class TflResponseInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        var response = chain.proceed(request)

        if (response.code == HttpURLConnection.HTTP_OK) {
            return Response.Builder().code(HttpURLConnection.HTTP_OK)
                .headers(response.headers).body(response.body)
                .request(request).protocol(Protocol.HTTP_2).message("").build()

        }

        Log.d(TAG, "intercept: ${response.body}")

        var newResponse = Response.Builder()
        for (header in response.headers) {
            Log.d(TAG, "intercept: $header")
            if (header.first.contains("x-request-404")) {
                Log.d(TAG, "intercept: ")
                continue
            } else {
                newResponse = newResponse.addHeader(header.first, header.second)
                Log.d(TAG, "intercept: Not404")
            }

        }
        return newResponse.body(response.body).code(HttpURLConnection.HTTP_OK)
            .headers(response.headers).body(response.body)
            .request(request).protocol(Protocol.HTTP_2).message("").build()

    }
}